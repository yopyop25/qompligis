import os
import tempfile
from collections import Counter
from pathlib import Path

import pytest

from QompliGIS.utils import (
    diff,
    dxf2gpkg,
    list_dxf_layers,
    list_gpkg_layers,
    list_shp_files,
)

DATAPATH = os.path.join(os.path.dirname(os.path.abspath(__file__)), "testdata")


def test_geopackage():
    gpkg_path = os.path.join(DATAPATH, "gpkg")
    orig = os.path.join(gpkg_path, "source.gpkg")
    comp = os.path.join(gpkg_path, "tocompare.gpkg")
    listOrig = list_gpkg_layers(orig)
    listComp = list_gpkg_layers(comp)
    assert listOrig == ["autre_ouvrage", "branchement", "cana"]
    assert listComp == ["autre_ouvrage", "branchement", "canalisation"]

    assert diff(listOrig, listComp) == Counter({"canalisation": 1, "cana": -1})
    assert diff(listComp, listOrig) == Counter({"cana": 1, "canalisation": -1})


def test_shp_folder():
    shp_path = os.path.join(DATAPATH, "shp")
    orig = os.path.join(shp_path, "source")
    comp = os.path.join(shp_path, "tocompare")
    listOrig = list_shp_files(orig)
    listComp = list_shp_files(comp)
    assert listOrig == ["autre_ouvrage", "branchement", "cana"]
    assert listComp == ["autre_ouvrage", "branchement"]

    assert diff(listOrig, listComp) == Counter({"cana": -1})
    assert diff(listComp, listOrig) == Counter({"cana": 1})


def test_dxf():
    gpkg_path = os.path.join(DATAPATH, "dxf")
    orig = os.path.join(gpkg_path, "source.dxf")
    comp = os.path.join(gpkg_path, "tocompare.dxf")
    listOrig = list_dxf_layers(orig)
    listComp = list_dxf_layers(comp)
    assert listOrig == ["autre_ouvrage", "branchement", "cana"]
    assert listComp == ["Calque1", "Cana"]

    assert diff(listOrig, listComp) == Counter(
        {"Cana": 1, "Calque1": 1, "autre_ouvrage": -1, "cana": -1, "branchement": -1}
    )
    assert diff(listComp, listOrig) == Counter(
        {"autre_ouvrage": 1, "cana": 1, "branchement": 1, "Cana": -1, "Calque1": -1}
    )


def test_dxfToGpkg():
    dxf_path = os.path.join(DATAPATH, "dxf")
    with tempfile.TemporaryDirectory() as tmp:
        comp = os.path.join(tmp, "dxf2gpkg.gpkg")
        dxf = os.path.join(dxf_path, "source.dxf")
        orig = os.path.join(dxf_path, "toGeoPackage.gpkg")
        listOrig = list_gpkg_layers(orig)

        dxf2gpkg(dxf, comp)

        listComp = list_gpkg_layers(comp)

        assert diff(listOrig, listComp) == Counter()
