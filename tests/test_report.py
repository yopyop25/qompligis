from pathlib import Path

import pytest

from QompliGIS.report import LayerSection, MainSection, QompliGISReport, ReportFormat


def test_QompliGISReport():
    """
    Comparison of generated report with a reference
    """

    report = QompliGISReport()
    report.addLayer("layer 1")
    report.addFieldInfo("layer 1", "super champ", "plutot correct")
    report.addLayerSection("layer 1", LayerSection.HOLES, "y'a des trous")
    report.addMainSection(MainSection.LAYER_LIST, "ok ok")
    with pytest.raises(ValueError) as e:
        report.addLayerSection("layer 1", LayerSection.FIELDS, "tentative")
    assert str(e.value) == "For FIELDS section, use addFieldInfo() method"
    with pytest.raises(KeyError) as e:
        report.addLayerSection("layer 2", LayerSection.CRS, "tentative")
    assert str(e.value) == "'The layer layer 2 is not present in the report'"
    assert (
        Path(__file__).parent / "testdata" / "test_report.html.ref"
    ).read_text() == report.report(
        ReportFormat.HTML
    ), "HTML report don't match with reference"
    assert (
        Path(__file__).parent / "testdata" / "test_report.md.ref"
    ).read_text() == report.report(
        ReportFormat.MARKDOWN
    ), "MARKDOWN report don't match with reference"
