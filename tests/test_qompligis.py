from QompliGIS import classFactory


def test_qompligis(qgis_iface):
    qompligis = classFactory(qgis_iface)
    qompligis.initGui()
    qompligis.compliance_check_action.trigger()
    qompligis.edit_configuration_action.trigger()
    qompligis.create_configuration_action.trigger()
