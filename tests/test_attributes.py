import pytest
from qgis.core import NULL, QgsFeature, QgsField, QgsFields
from qgis.PyQt.QtCore import QVariant

from QompliGIS.utils import has_null_attributes, list_null_attributes


def test_null_attributes():
    fields = QgsFields()
    fields.append(QgsField("id", QVariant.Int, "integer", len=10))
    fields.append(QgsField("name", QVariant.String, "string", len=10))
    feature = QgsFeature(fields)

    # [None, None]
    assert list_null_attributes(feature) == [True, True]
    assert has_null_attributes(feature) == True

    feature["name"] = NULL
    # [None, NULL]
    assert list_null_attributes(feature) == [True, True]
    assert has_null_attributes(feature) == True

    feature["id"] = 1
    # [1, NULL]
    assert list_null_attributes(feature) == [False, True]
    assert has_null_attributes(feature) == True

    feature["name"] = "test"
    # [1, 'test']
    assert list_null_attributes(feature) == [False, False]
    assert has_null_attributes(feature) == False
