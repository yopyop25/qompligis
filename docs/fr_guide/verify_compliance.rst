Vérification de la conformité
=============================

Utiliser l'icône |check_compliance_logo| pour ouvrir l'assistant de vérification de conformité.

#. Choisir le fichier de configuration de la conformité
#. Sélectionner le fichier (ou dossier dans le cas de shapefiles) de données
#. La page de rapport montre les étapes de vérifications en direct


.. image:: ../_static/images/fr/check_1.png
   :width: 50%

Le rapport est enregistré en choisissant un format et une destination sur la dernière page de l'assistant.

Les formats supportés sont :

- HTML
- Markdown

.. image:: ../_static/images/fr/check_2.png
   :width: 50%

.. figure:: ../_static/images/fr/check_3.png
   :width: 100%

   Aperçu d'un rapport en Markdown


.. |check_compliance_logo| image:: ../../QompliGIS/resources/images/checkCompliance.svg
   :width: 30 px
