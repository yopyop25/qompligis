Purpose of the plugin
=====================

General overview
----------------

This plugin aims to provide a simple way to verify if the structure of a dataset complies
with the structure of a reference dataset.

The supported dataset types are:

- geopackage
- folder of shapefile(s)
- DXF

How does it work?
-----------------

Prerequisite
............

A reference dataset is required.

Workflow
........

#. Create a compliance configuration file (template file) in which can be enabled many settings

   - CRS check
   - Curve and hole presence
   - Fields
   - etc.

#. Use the compliance configuration file and the dataset to verify to lauch the compliance check
#. Analyze the report
