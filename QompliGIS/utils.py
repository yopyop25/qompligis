"""
Utils functions and classes
"""

import re
from collections import Counter  # used for diff function
from enum import Enum, unique
from pathlib import Path
from typing import Any, Dict, List, Optional, Tuple, cast

import yaml
from qgis.core import (
    NULL,
    QgsAbstractGeometry,
    QgsDataProvider,
    QgsFeature,
    QgsFields,
    QgsGeometry,
    QgsProject,
    QgsTask,
    QgsVectorFileWriter,
    QgsVectorLayer,
    QgsWkbTypes,
)
from qgis.PyQt.QtCore import QObject, pyqtSignal

from QompliGIS import report
from QompliGIS.qt_utils import tr


def list_gpkg_info(filename: str) -> Dict[str, Tuple[str, List[Any]]]:
    """Return information of layers in GeoPackage filename

    Parameters
    ----------
    filename :
        GeoPackage filename

    Returns
    -------
    dict
        Returns dict tuple of {layer_name: (geom_type, list of fields)}
    """
    layer = QgsVectorLayer(filename, "test", "ogr")
    layers = layer.dataProvider().subLayers()
    info_dict: Dict[str, Tuple[str, List[Any]]] = dict()
    for layername in sorted(
        [l.split(QgsDataProvider.SUBLAYER_SEPARATOR)[1] for l in layers]
    ):
        my_layer = QgsVectorLayer(filename + "|layername=" + layername, "", "ogr")
        info_dict[layername] = (
            QgsWkbTypes.displayString(my_layer.wkbType()),
            my_layer.fields(),
        )
    return info_dict


def list_gpkg_vlayers(filename: str) -> List[QgsVectorLayer]:
    """Return list of layers in GeoPackage filename

    Parameters
    ----------
    filename :
        GeoPackage filename

    Returns
    -------
    list
        Returns a list QgsVectorLayer
    """
    layer = QgsVectorLayer(filename, "test", "ogr")
    layers = layer.dataProvider().subLayers()
    res = []
    for layername in sorted(
        [l.split(QgsDataProvider.SUBLAYER_SEPARATOR)[1] for l in layers]
    ):
        res.append(
            QgsVectorLayer(filename + "|layername=" + layername, layername, "ogr")
        )
    return res


def list_gpkg_layers(filename: str) -> List[str]:
    """Return list of layers in GeoPackage filename

    Parameters
    ----------
    filename :
        GeoPackage filename

    Returns
    -------
    list
        Returns a list of string
    """
    layer = QgsVectorLayer(filename, "test", "ogr")
    layers = layer.dataProvider().subLayers()
    return sorted([l.split(QgsDataProvider.SUBLAYER_SEPARATOR)[1] for l in layers])


def list_shp_info(folder: str) -> Dict[str, Tuple[str, List[Any]]]:
    """Return information of shapefile layers in folder

    Parameters
    ----------
    folder:
        Folder where ESRI Shapefile will be listed.

    Returns
    -------
    dict
        Returns dict tuple of {layer_name: (geom_type, list of fields)}
    """

    info_dict = {}
    for filepath in sorted(s for s in Path(folder).glob("*.shp")):
        my_layer = QgsVectorLayer(str(filepath), "", "ogr")
        info_dict[filepath.stem] = (
            QgsWkbTypes.displayString(my_layer.wkbType()),
            my_layer.fields(),
        )
    return info_dict


def list_shp_vlayers(folder: str) -> List[QgsVectorLayer]:
    """Returns list of shp layers in folder. This function will not check if the
    shp file is accompanied by other useful files (dbf, shx, idx, etc).

    Parameters
    ----------
    folder :
        Folder where ESRI Shapefile will be listed.

    Returns
    -------
    list
        Returns a list of QgsVectorLayer
    """
    res = []
    for filepath in sorted(s for s in Path(folder).glob("*.shp")):
        res.append(QgsVectorLayer(str(filepath), Path(filepath).stem, "ogr"))
    return res


def list_shp_files(folder: str) -> List[str]:
    """Returns list of shp files in folder. This function will not check if the
    shp file is accompanied by other useful files (dbf, shx, idx, etc).

    Parameters
    ----------
    folder :
        Folder where ESRI Shapefile will be listed.

    Returns
    -------
    list
        Returns a list of string
    """
    return sorted([s.stem for s in Path(folder).glob("*.shp")])


def list_dxf_info(filename: str) -> Dict[str, Tuple[str, List[Any]]]:
    """Return information of layers in DXF filename.

    Parameters
    ----------
    filename :
        DXF filename

    Returns
    -------
    list
        Returns dict tuple of {layer_name: (geom_type, list of fields)}
    """
    dxf = QgsVectorLayer(filename, "", "ogr")
    sublayers = dxf.dataProvider().subLayers()
    idx = dxf.fields().indexOf("Layer")
    values = list(dxf.uniqueValues(idx))
    info_dict: Dict[str, Tuple[str, List[Any]]] = dict()
    for sub in sublayers:
        geometry_type = sub.split(QgsDataProvider.SUBLAYER_SEPARATOR)[3]
        for layer in values:
            uri = "%s|layername=entities|geometrytype=%s|subset=Layer='%s'" % (
                filename,
                geometry_type,
                layer,
            )
            sub_vlayer = QgsVectorLayer(uri, "test", "ogr")
            if sub_vlayer.featureCount() > 0:
                info_dict[layer] = (geometry_type, [])
    return info_dict


def list_dxf_vlayers(filename: str) -> List[QgsVectorLayer]:
    """Return list of layers in DXF filename.

    Parameters
    ----------
    filename :
        DXF filename

    Returns
    -------
    list
        Returns a list of QgsVectorLayer
    """
    dxf = QgsVectorLayer(filename, "test", "ogr")
    sublayers = dxf.dataProvider().subLayers()
    idx = dxf.fields().indexOf("Layer")
    values = list(dxf.uniqueValues(idx))
    res = []
    for sub in sublayers:
        geometry_type = sub.split(QgsDataProvider.SUBLAYER_SEPARATOR)[3]
        for layer in values:
            uri = "%s|layername=entities|geometrytype=%s|subset=Layer='%s'" % (
                filename,
                geometry_type,
                layer,
            )
            sub_vlayer = QgsVectorLayer(uri, layer, "ogr")
            if sub_vlayer.featureCount() > 0:
                res.append(sub_vlayer)
    return res


def list_dxf_layers(filename: str) -> List[str]:
    """Return list of layers in DXF filename.

    Parameters
    ----------
    filename :
        DXF filename

    Returns
    -------
    list
        Returns a list of string
    """
    dxf = QgsVectorLayer(filename, "test", "ogr")
    idx = dxf.fields().indexOf("Layer")
    return sorted(list(dxf.uniqueValues(idx)))


def dxf2gpkg(dxf_file: str, gpkg_file: str) -> Dict[str, str]:
    """From an input DXF, returns an output GPKG.
    A table will be created for each Layer and GeometryType from the DXF.
    A DXF layer named "test" containing points and lines will be converted into
    two tables test_Point and test_LineString in GPKG.

    Parameters
    ----------
    dxf_file :
        Input file (DXF)
    gpkg_file :
        Output file (GPKG)

    Returns
    -------
    dict
        Returns a dictionnary of the output of each result of
        QgsVectorFileWriter.writeAsVectorFormatV2
    """
    ret: Dict[str, str] = dict()

    dxf = QgsVectorLayer(dxf_file, "test", "ogr")
    sublayers = dxf.dataProvider().subLayers()
    idx = dxf.fields().indexOf("Layer")
    values = list(dxf.uniqueValues(idx))
    for sub in sublayers:
        geometry_type = sub.split(QgsDataProvider.SUBLAYER_SEPARATOR)[3]
        for layer in values:
            uri = "%s|layername=entities|geometrytype=%s|subset=Layer='%s'" % (
                dxf_file,
                geometry_type,
                layer,
            )
            layername = "%s_%s" % (layer, geometry_type)
            sub_vlayer = QgsVectorLayer(uri, layername, "ogr")
            if sub_vlayer.featureCount() > 0:
                save_options = QgsVectorFileWriter.SaveVectorOptions()
                save_options.layerName = layername
                if len(ret) > 0:
                    save_options.actionOnExistingFile = (
                        QgsVectorFileWriter.CreateOrOverwriteLayer
                    )
                transform_context = QgsProject.instance().transformContext()
                error = QgsVectorFileWriter.writeAsVectorFormatV2(
                    sub_vlayer, gpkg_file, transform_context, save_options
                )
                ret[layername] = error
    return ret


def diff(source: List[Any], compare: List[Any]) -> Counter:
    """Compare lists using Counter.

    From https://stackoverflow.com/a/34345375

    Parameters
    ----------
    a :
        Source list
    b :
        List to compare against source

    Examples
    --------
    >>> diff([1, 2], [1,2,3])
    Counter({3: 1})
    >>> diff([1, 2, 3], [1,2])
    Counter({3: -1})
    >>> diff([1, 2, 3], [1, 2, 3])
    Counter()
    >>> diff([1, 2, 3], [1, 3, 2])
    Counter()
    """
    c_source, c_compare = Counter(source), Counter(compare)
    to_add = c_compare - c_source
    to_remove = c_source - c_compare
    changes = Counter(to_add)
    changes.subtract(to_remove)
    return changes


@unique
class InputFormat(Enum):
    SHAPEFILES = "Shapefiles"
    GEOPACKAGE = "Geopackage (*.gpkg)"
    DXF = "DAO (*.dxf)"


@unique
class FieldsOrder(Enum):
    """FieldsOrder."""

    SAMEORDER = 1
    ORDERDIFFER = 2
    FIELDSDIFFER = 3


@unique
class FieldsComparison(Enum):
    """FieldsComparison."""

    NOTPRESENT = -1
    TYPEDIFFER = -2
    PRECISIONDIFFER = -3
    LENGTHDIFFER = -4
    OTHERDIFFERENCE = 5
    SAMEFIELD = 1


def compare_fields(
    origin_fields: QgsFields, comp_fields: QgsFields
) -> Tuple[bool, Dict[str, Any]]:
    """Compare fields and returns informations on mismatch fields.

    It will compare two lists of fields and return a dictionary containing
    information about the order of the fields (identical, identical but not
    in the same order or different)

    For each field present in the original list, we will compare the
    information and return an information about the difference between
    these fields.

    Parameters
    ----------
    origin_fields :
        source QgsFields
    comp_fields :
        QgsFields to compare against source

    Returns
    -------
    tuple(bool, dict)
        Returns a tuple(boolean, dictionnary) with several informations.

        - (True, dict()) When origin_fields == comp_fields
        - (False, {'fieldsOrder': (FieldsOrder enum, Counter),
                   'fieldsComparison': {fieldname: (FieldsComparison enum,
                   Information)}})

    """
    result: Dict[str, Any] = dict()
    # the list is expected to be identical and ordered

    if origin_fields == comp_fields:
        return (True, result)

    # We will check field by field
    # First step is to compare the field names
    if origin_fields.names() == comp_fields.names():
        result["fieldsOrder"] = (FieldsOrder.SAMEORDER, Counter())
    else:
        # However, they may be in a different order.
        counter_fields_sorted = diff(origin_fields.names(), comp_fields.names())
        if counter_fields_sorted == Counter():
            result["fieldsOrder"] = (FieldsOrder.ORDERDIFFER, counter_fields_sorted)
        else:
            result["fieldsOrder"] = (FieldsOrder.FIELDSDIFFER, counter_fields_sorted)
    # we check for each field of the origin if it is present in the list to compare
    # and see what changes
    fields_result: Dict[str, Any] = dict()
    for i, fieldname in enumerate(origin_fields.names()):
        idx = comp_fields.indexFromName(fieldname)
        if idx == -1:
            fields_result[fieldname] = (FieldsComparison.NOTPRESENT,)
        else:
            origin_field = origin_fields[i]
            compare_field = comp_fields[idx]
            if origin_field == compare_field:
                fields_result[fieldname] = (FieldsComparison.SAMEFIELD,)
            else:
                types_tuple = (
                    (
                        origin_field.typeName(),
                        origin_field.length(),
                        origin_field.precision(),
                    ),
                    (
                        compare_field.typeName(),
                        compare_field.length(),
                        compare_field.precision(),
                    ),
                )
                if origin_field.type() != compare_field.type():
                    fields_result[fieldname] = (
                        FieldsComparison.TYPEDIFFER,
                        types_tuple,
                    )
                elif origin_field.precision() != compare_field.precision():
                    fields_result[fieldname] = (
                        FieldsComparison.PRECISIONDIFFER,
                        (origin_field.precision(), compare_field.precision()),
                    )
                elif origin_field.length() != compare_field.length():
                    fields_result[fieldname] = (
                        FieldsComparison.LENGTHDIFFER,
                        (origin_field.length(), compare_field.length()),
                    )
                else:
                    fields_result[fieldname] = (
                        FieldsComparison.OTHERDIFFERENCE,
                        types_tuple,
                    )
            result["fieldsComparison"] = fields_result
    return (False, result)


def geometry_type_compare(
    origin_layer: QgsVectorLayer, compare_layer: QgsVectorLayer
) -> Tuple[bool, str, str]:
    """Compare geometry type of origin_layer and compare_layer.

    eg: origin_layer is a PointZM and compare_layer a Point, will return
    (False, 'PointZM', 'Point')

    Parameters
    ----------
    origin_layer :
        Source QgsVectorLayer
    compare_layer :
        QgsVectorLayer to compare against source

    Returns
    -------
    tuple(bool, str, str)
        - origin_layer wkbTypes == compare_layer wkbTypes
        - origin_layer display string
        - compare_layer display string
    """
    origin_display_string = QgsWkbTypes.displayString(origin_layer.wkbType())
    compare_display_string = QgsWkbTypes.displayString(compare_layer.wkbType())

    return (
        origin_display_string == compare_display_string,
        origin_display_string,
        compare_display_string,
    )


def flat_geometry_type_compare(
    origin_layer: QgsVectorLayer, compare_layer: QgsVectorLayer
) -> Tuple[bool, str, str]:
    """Compare the flat geometry type of origin_layer and compare_layer.

    eg: origin_layer is a PointZM and compare_layer a Point, will return
    (True, 'Point', 'Point')

    Parameters
    ----------
    origin_layer :
        Source QgsVectorLayer
    compare_layer :
        QgsVectorLayer to compare against source

    Returns
    -------
    tuple(bool, str, str)
        - origin_layer geometry type == compare_layer geometry type
        - origin_layer geometry display string
        - compare_layer geometry display string
    """
    origin_geom_display_string = QgsWkbTypes.geometryDisplayString(
        origin_layer.geometryType()
    )
    compare_geom_display_string = QgsWkbTypes.geometryDisplayString(
        compare_layer.geometryType()
    )

    return (
        origin_geom_display_string == compare_geom_display_string,
        origin_geom_display_string,
        compare_geom_display_string,
    )


def crs_compare(
    origin_layer: QgsVectorLayer, compare_layer: QgsVectorLayer
) -> Tuple[bool, str, str]:
    """Compare the CRS of origin_layer and compare_layer.

    Parameters
    ----------
    origin_layer :
        Source QgsVectorLayer
    compare_layer :
        QgsVectorLayer to compare against source

    Returns
    -------
    tuple(bool, str, str)
        - origin_layer crs == compare_layer crs
        - origin_layer auth id string
        - compare_layer auth id string
    """
    origin_crs = origin_layer.sourceCrs()
    compare_crs = compare_layer.sourceCrs()
    return (origin_crs == compare_crs, origin_crs.authid(), compare_crs.authid())


def has_minimal_length(geometry: QgsGeometry, min_length: float) -> bool:
    """Returns true if the length of the line is superior to min_length.

    Parameters
    ----------
    geometry :
        QgsGeometry make sense for LineString type
    min_length :
        Minimal length
    """
    return cast(bool, geometry.length() >= min_length)


def has_minimal_area(geometry: QgsGeometry, min_area: float) -> bool:
    """Returns true if the area of the polygon is superior to min_area.

    Parameters
    ----------
    geometry :
        QgsGeometry make sense for Polygon type
    min_area :
        Minimal area
    """
    return cast(bool, geometry.area() >= min_area)


def has_holes(abstract_geometry: QgsAbstractGeometry) -> bool:
    """Returns true if the polygon contains holes aka have interior rings.

    Parameters
    ----------
    abstract_geometry :
        QgsAbstractGeometry make sense for Polygon type
    """
    return cast(bool, abstract_geometry.numInteriorRings() > 0)


def is_valid(abstract_geometry: QgsAbstractGeometry) -> bool:
    """Returns a tuple (boolean, reaason).
        boolean: the polygon is valid (OGC sense)

    Parameters
    ----------
    abstract_geometry :
        QgsAbstractGeometry make sense for Polygon type
    """
    return cast(bool, abstract_geometry.isValid())


def has_curved_segments(abstract_geometry: QgsAbstractGeometry) -> bool:
    """Returns true if the geometry has curved segments.

    Parameters
    ----------
    abstract_geometry :
        QgsAbstractGeometry
    """
    return cast(bool, abstract_geometry.hasCurvedSegments())


def list_null_attributes(feature: QgsFeature) -> List[bool]:
    """Returns a list of boolean with the NULL/None comparison for each fields.
    [1, NULL, 'test'] -> [False, True, False]

    Parameters
    ----------
    feature :
        QgsFeature
    """
    return [x == NULL or x is None for x in feature.attributes()]


def has_null_attributes(feature: QgsFeature) -> bool:
    """Returns true if at least one attribute in feature == NULL or None.

    see list_null_attributes.

    Parameters
    ----------
    feature :
        QgsFeature
    """
    return any(list_null_attributes(feature))


class DoVerif(QgsTask):

    log_msg = pyqtSignal(str)

    def __init__(
        self,
        conf_dict: Dict[str, Any],
        data_path: str,
        parent: Optional[QObject] = None,
    ):
        super().__init__(parent)
        self.conf_dict = conf_dict
        self.data_path = data_path
        self.run_report = report.QompliGISReport()

    def run(self) -> bool:
        ref_filepath = self.conf_dict["filepath"]
        if not Path(ref_filepath).exists():
            self.log_msg.emit(tr("The path {0} does not exist!").format(ref_filepath))
            return False

        ref_layer_list: List[QgsVectorLayer] = []
        input_format = InputFormat(self.conf_dict["input_format"])

        detected_file_type = None
        if Path(self.data_path).is_dir():
            detected_file_type = InputFormat.SHAPEFILES
        else:
            for file_type in InputFormat:
                if file_type == InputFormat.SHAPEFILES:
                    continue
                if Path(self.data_path).suffix == re.search(r"\(\*(.+)\)", file_type.value)[1]:  # type: ignore
                    detected_file_type = file_type

        if detected_file_type != input_format:
            raise ValueError(
                tr(
                    "The data format to check ({0}) does not correspond with the reference data ({1})"
                ).format(detected_file_type.value, input_format.value)
            )

        # Shapefile
        if input_format == InputFormat.SHAPEFILES:
            layer_list = list_shp_vlayers(self.data_path)
            ref_layer_list = list_shp_vlayers(ref_filepath)

        # CAD or DXF
        elif input_format == InputFormat.DXF:
            layer_list = list_dxf_vlayers(self.data_path)
            ref_layer_list = list_dxf_vlayers(ref_filepath)

        # GPKG
        elif input_format == InputFormat.GEOPACKAGE:
            layer_list = list_gpkg_vlayers(self.data_path)
            ref_layer_list = list_gpkg_vlayers(ref_filepath)

        if len(ref_layer_list) == 0:
            self.log_msg.emit(tr("No layer found in reference file!"))
            return False

        # Layer list verification
        self.log_msg.emit(tr("Checking layer list..."))
        res = diff([a.name() for a in ref_layer_list], [a.name() for a in layer_list])
        if len(res) == 0:
            msg = tr("OK: Layers comply with the reference")
            self.run_report.addMainSection(report.MainSection.LAYER_LIST, msg)
            self.log_msg.emit(msg)
        else:
            for layername, occurrence in res.items():
                if occurrence < 0:
                    msg = tr("Layer '{0}' is missing in dataset").format(layername)
                    self.run_report.addMainSection(report.MainSection.LAYER_LIST, msg)
                    self.log_msg.emit(msg)
                else:
                    msg = tr(
                        "Layer '{0}' found but not present in reference dataset"
                    ).format(layername)
                    self.run_report.addMainSection(report.MainSection.LAYER_LIST, msg)
                    self.log_msg.emit(msg)

        # Layer verification
        for layer in layer_list:
            layer_name = layer.name()
            if layer_name not in self.conf_dict.keys():
                continue
            for ref_layer in ref_layer_list:
                if ref_layer.name() == layer_name:
                    break

            self.run_report.addLayer(layer_name)
            self.log_msg.emit(tr("Checking layer {0}").format(layer_name))

            # CRS
            if self.conf_dict[layer_name]["strict_crs"]:
                msg = tr("Checking CRS...")
                self.log_msg.emit(msg)
                new_crs = layer.sourceCrs().authid()
                ref_crs = ref_layer.sourceCrs().authid()
                if new_crs == ref_crs:
                    msg = tr("OK")
                else:
                    msg = tr("CRS is {new_crs} but must be {ref_crs}")
                self.run_report.addLayerSection(
                    layer_name, report.LayerSection.CRS, msg
                )
                self.log_msg.emit(msg)
            else:
                self.run_report.addLayerSection(
                    layer_name, report.LayerSection.CRS, tr("No CRS check asked")
                )

            # Fields
            if self.conf_dict[layer_name]["strict_fields"]:
                self.log_msg.emit(tr("Checking fields..."))
                fields = [a.name() for a in layer.fields()]
                ref_fields = [a.name() for a in ref_layer.fields()]
                ok, dict_err = compare_fields(ref_layer.fields(), layer.fields())
                if ok:
                    msg = tr("OK")
                    self.run_report.addFieldInfo(layer_name, "", msg)
                    self.log_msg.emit(msg)
                else:
                    field_order, counter = dict_err["fieldsOrder"]
                    if field_order == FieldsOrder.FIELDSDIFFER:
                        general_msg = tr(
                            "Differences found on fields: see each field information."
                        )
                        for field, count in counter.items():
                            if count == -1:
                                msg = tr("Missing")
                                if self.conf_dict[layer_name]["fields"][field][
                                    "mandatory"
                                ]:
                                    msg += tr(" - AND MANDATORY!")
                            elif count == 1:
                                msg = tr("Not in reference layer.")
                            self.run_report.addFieldInfo(layer_name, field, msg)
                            self.log_msg.emit(tr("Field {0}: {1}").format(field, msg))
                    elif field_order == FieldsOrder.ORDERDIFFER:
                        general_msg = tr(
                            "Fields are in a different order: found {0} but reference layer order is {1}."
                        ).format(fields, ref_fields)
                    elif field_order == FieldsOrder.SAMEORDER:
                        general_msg = tr(
                            "Fields are all present and in the same order: see each field information."
                        )
                    self.run_report.addFieldInfo(layer_name, "", general_msg)
                    self.log_msg.emit(general_msg)
                    for field, tuple_comparison in dict_err["fieldsComparison"].items():
                        msg = ""
                        if tuple_comparison[0] == FieldsComparison.LENGTHDIFFER:
                            data = tuple_comparison[1]
                            msg = tr(
                                "Length differ: found {0} but reference is {1}."
                            ).format(data[1], data[0])
                        elif tuple_comparison[0] == FieldsComparison.PRECISIONDIFFER:
                            data = tuple_comparison[1]
                            msg = tr(
                                "Precision differ: found {0} but reference is {1}."
                            ).format(data[1], data[0])
                        elif tuple_comparison[0] == FieldsComparison.TYPEDIFFER:
                            data = tuple_comparison[1]
                            msg = tr(
                                "Type differ: found (type: {0}, length: {1}, precision: {2}) "
                                "but reference is (type: {3}, length: {4}, precision: {5})."
                            ).format(
                                data[1][0],
                                data[1][1],
                                data[1][2],
                                data[0][0],
                                data[0][1],
                                data[0][2],
                            )
                        elif tuple_comparison == FieldsComparison.OTHERDIFFERENCE:
                            data = tuple_comparison[1]
                            msg = tr(
                                "Other difference... found informations are type: {0}, length: {1}, precision: {2} "
                                "and reference informations are type: {3}, length: {4}, precision: {5}."
                            ).format(
                                data[1][0],
                                data[1][1],
                                data[1][2],
                                data[0][0],
                                data[0][1],
                                data[0][2],
                            )
                        elif tuple_comparison == FieldsComparison.SAMEFIELD:
                            msg = tr("Fields are the same.")
                        if msg != "":
                            self.run_report.addFieldInfo(layer_name, field, msg)
                            self.log_msg.emit(f"Field {field}: {msg}")
            else:
                msg = tr("No strict check on fields")
                self.run_report.addFieldInfo(layer_name, msg, "")
                self.log_msg.emit(msg)

            # Curves, holes, minimum area, and minimum length
            check_curves = (
                "curves" in self.conf_dict[layer_name]
                and not self.conf_dict[layer_name]["curves"]
            )
            check_holes = (
                "holes" in self.conf_dict[layer_name]
                and not self.conf_dict[layer_name]["holes"]
            )
            check_min_area = (
                "min_area" in self.conf_dict[layer_name]
                and self.conf_dict[layer_name]["min_area"] > 0
            )
            check_min_length = (
                "min_length" in self.conf_dict[layer_name]
                and self.conf_dict[layer_name]["min_length"] > 0
            )
            check_null_fields = self.conf_dict[layer_name]["strict_fields"]
            if check_min_length:
                self.log_msg.emit(tr("Checking minimum length..."))
                min_length = self.conf_dict[layer_name]["min_length"]
            elif "min_length" in self.conf_dict[layer_name]:
                self.run_report.addLayerSection(
                    layer_name,
                    report.LayerSection.MIN_LENGTH,
                    tr("No minimum length check asked"),
                )
            if check_min_area:
                self.log_msg.emit(tr("Checking minimum area..."))
                min_area = self.conf_dict[layer_name]["min_area"]
            elif "min_area" in self.conf_dict[layer_name]:
                self.run_report.addLayerSection(
                    layer_name,
                    report.LayerSection.MIN_AREA,
                    tr("No minimum area check asked"),
                )
            if check_curves:
                self.log_msg.emit(tr("Checking curves..."))
            elif "curves" in self.conf_dict[layer_name]:
                self.run_report.addLayerSection(
                    layer_name,
                    report.LayerSection.CURVES,
                    tr("No curves check asked"),
                )
            if check_holes:
                self.log_msg.emit(tr("Checking holes..."))
            elif "holes" in self.conf_dict[layer_name]:
                self.run_report.addLayerSection(
                    layer_name,
                    report.LayerSection.HOLES,
                    tr("No holes check asked"),
                )
            if check_null_fields:
                self.log_msg.emit(tr("Checking NULL fields..."))
            min_length_ok, min_area_ok, curves_ok, holes_ok = 4 * [True]
            for i, feature in enumerate(layer.getFeatures()):
                abstract_geometry = feature.geometry().constGet()
                if check_min_length:
                    if not has_minimal_length(feature.geometry(), min_length):
                        msg = tr(
                            "Feature {0} has a length {1} < minimum length {2}"
                        ).format(i, round(feature.geometry().length(), 2), min_length)
                        self.run_report.addLayerSection(
                            layer_name, report.LayerSection.MIN_LENGTH, msg
                        )
                        self.log_msg.emit(msg)
                        min_length_ok = False
                if check_min_area:
                    if not has_minimal_area(feature.geometry(), min_area):
                        msg = tr(
                            "Feature {0} has an area {1} < minimum area {2}"
                        ).format(i, round(feature.geometry().area(), 2), min_area)
                        self.run_report.addLayerSection(
                            layer_name, report.LayerSection.MIN_AREA, msg
                        )
                        self.log_msg.emit(msg)
                        min_area_ok = False
                if check_curves:
                    if has_curved_segments(abstract_geometry):
                        msg = tr("Feature {0} contains curve(s)").format(i)
                        self.run_report.addLayerSection(
                            layer_name, report.LayerSection.CURVES, msg
                        )
                        self.log_msg.emit(msg)
                        curves_ok = False
                if check_holes:
                    if has_holes(abstract_geometry):
                        msg = tr("Feature {0} contains hole(s)").format(i)
                        self.run_report.addLayerSection(
                            layer_name, report.LayerSection.HOLES, msg
                        )
                        self.log_msg.emit(msg)
                        holes_ok = False
                if check_null_fields:
                    null_attr_idxs = [
                        a
                        for a, is_null in enumerate(list_null_attributes(feature))
                        if is_null
                    ]
                    for null_attr_idx in null_attr_idxs:
                        field_name = layer.fields().at(null_attr_idx).name()
                        if self.conf_dict[layer_name]["fields"][field_name][
                            "mandatory"
                        ]:
                            msg = tr("Mandatory field is NULL in feature {0}").format(i)
                            self.run_report.addFieldInfo(
                                layer_name,
                                field_name,
                                msg,
                            )
                            self.log_msg.emit(msg)
            if check_min_length and min_length_ok:
                self.run_report.addLayerSection(
                    layer_name, report.LayerSection.MIN_LENGTH, "OK"
                )
                self.log_msg.emit(tr("OK: layer complies with minimum length"))
            if check_min_area and min_area_ok:
                self.run_report.addLayerSection(
                    layer_name, report.LayerSection.MIN_AREA, "OK"
                )
                self.log_msg.emit(tr("OK: layer complies with minimum area"))
            if check_curves and curves_ok:
                self.run_report.addLayerSection(
                    layer_name, report.LayerSection.CURVES, "OK"
                )
                self.log_msg.emit(tr("OK: layer does not have curve"))
            if check_holes and holes_ok:
                self.run_report.addLayerSection(
                    layer_name, report.LayerSection.HOLES, "OK"
                )
                self.log_msg.emit(tr("OK: layer does not have hole"))

        return True


def get_input_format(config_filepath: Path) -> InputFormat:
    with config_filepath.open() as filepath:
        conf_dict = yaml.full_load(filepath)
    return InputFormat(conf_dict["input_format"])


def check_conf_dict_format(conf_dict: Dict[str, Any]) -> str:
    # Check main sections
    main_sections = ["input_format", "filepath"]
    missing_sections = []
    for section in main_sections:
        if section not in conf_dict:
            missing_sections.append(section)
    if len(missing_sections) == 0:
        # If ok, check layer sections
        for layer, value in conf_dict.items():
            if layer in main_sections:
                continue
            dim = QgsWkbTypes.wkbDimensions(
                QgsWkbTypes.parseType(value["geometry_type"])
            )
            layer_sections = [
                "strict_crs",
                "strict_fields",
                "strict_geometry",
                "fields",
                "geometry_type",
            ]
            if dim == 1:
                layer_sections += ["curves", "min_length"]
            elif dim == 2:
                layer_sections += ["holes", "curves", "min_area"]
            for layer_section in layer_sections:
                if layer_section not in value:
                    missing_sections.append(layer_section)

    if len(missing_sections) > 0:
        return tr("Bad formed configuration file: missing section(s) {0}").format(
            ", ".join(missing_sections)
        )
    return ""
