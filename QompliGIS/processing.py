"""
QompliGIS GPKG provider
"""
from pathlib import Path

import processing
import yaml
from qgis.core import (
    QgsApplication,
    QgsProcessing,
    QgsProcessingAlgorithm,
    QgsProcessingException,
    QgsProcessingMultiStepFeedback,
    QgsProcessingParameterEnum,
    QgsProcessingParameterFile,
)
from qgis.PyQt.QtWidgets import QTextEdit

from QompliGIS.report import ReportFormat
from QompliGIS.utils import DoVerif, InputFormat, tr


class QompliGISProcessing(QgsProcessingAlgorithm):
    def initAlgorithm(self, config=None):
        self.addParameter(
            QgsProcessingParameterFile(
                "conf",
                tr("Configuration file"),
                behavior=QgsProcessingParameterFile.File,
                fileFilter="YAML (*.yaml)",
                defaultValue=None,
            )
        )
        self.addParameter(
            QgsProcessingParameterFile(
                "foldertocheck",
                tr("Data folder to check (for Shapefiles)"),
                behavior=QgsProcessingParameterFile.Folder,
                optional=True,
            )
        )
        self.addParameter(
            QgsProcessingParameterFile(
                "filetocheck",
                tr("Data file to check"),
                behavior=QgsProcessingParameterFile.File,
                fileFilter=";;".join([a.value for a in InputFormat]),
                optional=True,
            )
        )
        self.addParameter(
            QgsProcessingParameterEnum(
                "report_format",
                tr("Report format"),
                options=["Markdown", "HTML"],
                allowMultiple=False,
                defaultValue=[],
            )
        )
        self.addParameter(
            QgsProcessingParameterFile(
                "report_path",
                tr("Report path"),
                behavior=QgsProcessingParameterFile.File,
                defaultValue=None,
            )
        )

    def processAlgorithm(self, parameters, context, model_feedback):
        # Use a multi-step feedback, so that individual child algorithm progress reports are adjusted for the
        # overall progress through the model
        feedback = QgsProcessingMultiStepFeedback(0, model_feedback)

        confpath = self.parameterAsFile(parameters, "conf", context)
        filepath = self.parameterAsFile(parameters, "filetocheck", context)
        folderpath = self.parameterAsFile(parameters, "foldertocheck", context)
        if not (filepath or folderpath):
            raise QgsProcessingException(
                tr(
                    "A data file or a data folder to check (for Shapefiles) must be supplied"
                )
            )
        with Path(confpath).open() as conf:
            conf_dict = yaml.full_load(conf)
        self.worker = DoVerif(conf_dict, filepath or folderpath)
        self.worker.run()

        report_path = self.parameterAsFile(parameters, "report_path", context)
        report_format = self.parameterAsEnum(parameters, "report_format", context)
        with Path(report_path).open("w") as fileout:
            if report_format == 0:
                fileout.write(self.worker.run_report.report(ReportFormat.MARKDOWN))
            elif report_format == 1:
                fileout.write(self.worker.run_report.report(ReportFormat.HTML))
        results = {"log": tr("Saved to ") + report_path}

        return results

    def name(self):
        return "check_compliance"

    def displayName(self):
        return tr("Check compliance")

    def group(self):
        return "QompliGIS"

    def groupId(self):
        return "QompliGIS"

    def createInstance(self):
        return QompliGISProcessing()
