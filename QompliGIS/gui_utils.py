"""
All wizard pages classes
"""

import re
from enum import Enum
from pathlib import Path
from typing import Any, Dict, Optional

import yaml
from qgis.core import QgsApplication, QgsVectorFileWriter, QgsWkbTypes
from qgis.gui import QgsFileWidget
from qgis.PyQt import uic
from qgis.PyQt.QtCore import Qt
from qgis.PyQt.QtWidgets import (
    QComboBox,
    QFormLayout,
    QHBoxLayout,
    QLabel,
    QListWidget,
    QMessageBox,
    QStackedWidget,
    QTableWidgetItem,
    QTextEdit,
    QVBoxLayout,
    QWidget,
    QWizard,
    QWizardPage,
)

from QompliGIS.qt_utils import boolToQtCheckState, qtCheckStateToBool, tr
from QompliGIS.report import ReportFormat
from QompliGIS.utils import (
    DoVerif,
    InputFormat,
    check_conf_dict_format,
    get_input_format,
    list_dxf_info,
    list_gpkg_info,
    list_shp_info,
)


class DataInputPage(QWizardPage):
    """
    Page to select the data that will be used as a reference
    to create the configuration file
    """

    def __init__(self, parent: Optional[QWidget] = None):
        super().__init__(parent)
        self.setTitle(tr("Choose the dataset to use as a reference"))
        cbx_input_format = QComboBox()
        cbx_input_format.addItems(
            ["", InputFormat.SHAPEFILES.value, InputFormat.DXF.value]
        )
        if "gpkg" in QgsVectorFileWriter.supportedFormatExtensions():
            cbx_input_format.addItem(InputFormat.GEOPACKAGE.value)
        filepath = QgsFileWidget()
        lbl = QLabel(tr("Choose a file"))

        def textChanged(text: str) -> None:
            """
            Change the behavior or the file selector according to
            the file type
            """
            if text == InputFormat.SHAPEFILES.value:
                filepath.setStorageMode(QgsFileWidget.StorageMode.GetDirectory)
                lbl.setText(tr("Choose a folder"))
            else:
                filepath.setStorageMode(QgsFileWidget.StorageMode.GetFile)
                lbl.setText(tr("Choose a file"))
                filepath.setFilter(text)

        textChanged(cbx_input_format.currentText())
        cbx_input_format.currentTextChanged.connect(textChanged)
        layout = QFormLayout()
        layout.addRow(tr("Format"), cbx_input_format)
        layout.addRow(lbl, filepath)
        self.setLayout(layout)
        self.registerField(
            "input_format*",
            cbx_input_format,
            "currentText",
            cbx_input_format.currentTextChanged,
        )
        self.registerField("filepath*", filepath.lineEdit())

    def validatePage(self):
        filepath = Path(self.field("filepath"))
        if not filepath.exists():
            QMessageBox.warning(
                None,
                tr("Warning"),
                tr("The path {0} does not exist!").format(filepath),
            )
            return False
        return True


class VerifDataInputPage(QWizardPage):
    """
    Page to select the data to be checked
    """

    def __init__(self, parent: Optional[QWidget] = None):
        super().__init__(parent)
        self.setTitle(tr("Choose the dataset to check"))
        self.filepath = QgsFileWidget()
        self.lbl = QLabel()
        layout = QFormLayout()
        layout.addRow(self.lbl, self.filepath)
        self.setLayout(layout)
        self.registerField("filepath*", self.filepath.lineEdit())

    def initializePage(self):
        input_format = get_input_format(Path(self.field("config_file")))
        if input_format == InputFormat.SHAPEFILES:
            self.filepath.setStorageMode(QgsFileWidget.StorageMode.GetDirectory)
            self.lbl.setText(tr("Choose a folder with your shapefiles"))
        else:
            self.filepath.setStorageMode(QgsFileWidget.StorageMode.GetFile)
            self.lbl.setText(tr("Choose a file - {0}").format(input_format.value))
            self.filepath.setFilter(input_format.value)

    def validatePage(self):
        if not Path(self.filepath.filePath()).exists():
            QMessageBox.warning(
                None,
                tr("Warning"),
                tr("The path {0} does not exist!").format(self.filepath.filePath()),
            )
            return False
        return True


class ConfigEditPage(QWizardPage):
    """
    Page to configure the verification constraints
    while editing an existing configuration
    """

    def __init__(self, parent: Optional[QWidget] = None):
        super().__init__(parent)
        self.setTitle(tr("Constraints configuration"))
        page_layout = QHBoxLayout()
        self.setLayout(page_layout)
        self.stack = QStackedWidget()
        self.layers_view = QListWidget()
        self.layers_view.setMinimumWidth(150)
        self.layers_view.setMaximumWidth(300)
        self.conf_dict: Dict[str, Any] = {}
        page_layout.addWidget(self.layers_view)
        page_layout.addWidget(self.stack)

        self.layers_view.currentRowChanged.connect(self.stack.setCurrentIndex)

    def initializePage(self):
        with Path(self.field("config_file")).open() as filein:
            self.conf_dict = yaml.full_load(filein)

        input_format = InputFormat(self.conf_dict["input_format"])
        filepath = self.conf_dict["filepath"]

        # Shapefile
        if input_format == InputFormat.SHAPEFILES:
            info_list = list_shp_info(filepath)

        # CAD or DXF
        elif input_format == InputFormat.DXF:
            info_list = list_dxf_info(filepath)

        # GPKG
        elif input_format == InputFormat.GEOPACKAGE:
            info_list = list_gpkg_info(filepath)

        for layer, (geom_type, fields) in info_list.items():
            field_dict = self.conf_dict[layer]["fields"]
            wid = uic.loadUi(Path(__file__).resolve(True).parent / "conf_wid.ui")
            wid.tbl_fields.setRowCount(len(fields))
            dim = QgsWkbTypes.wkbDimensions(QgsWkbTypes.parseType(geom_type))
            for i, field in enumerate(fields):
                field_name = field.name()
                item_type = QTableWidgetItem(field_name)
                wid.tbl_fields.setItem(i, 0, item_type)
                checkbox = QTableWidgetItem()
                checkbox.setData(
                    Qt.CheckStateRole,
                    Qt.Checked if field_dict[field_name]["mandatory"] else Qt.Unchecked,
                )
                checkbox.setFlags(checkbox.flags() & ~Qt.ItemIsEditable)
                wid.tbl_fields.setItem(i, 1, checkbox)

            # Layer constraints
            if dim == 0:
                pass
            elif dim == 1:
                wid.chbx_curves_line.setCheckState(
                    boolToQtCheckState(self.conf_dict[layer]["curves"])
                )
                wid.spb_min_length.setValue(self.conf_dict[layer]["min_length"])
            elif dim == 2:
                wid.chbx_holes.setCheckState(
                    boolToQtCheckState(self.conf_dict[layer]["holes"])
                )
                wid.chbx_curves_poly.setCheckState(
                    boolToQtCheckState(self.conf_dict[layer]["curves"])
                )
                wid.spb_min_area.setValue(self.conf_dict[layer]["min_area"])
            wid.chbx_strict_geom.setCheckState(
                boolToQtCheckState(self.conf_dict[layer]["strict_geometry"])
            )
            wid.chbx_strict_crs.setCheckState(
                boolToQtCheckState(self.conf_dict[layer]["strict_crs"])
            )
            wid.chbx_strict_fields.setCheckState(
                boolToQtCheckState(self.conf_dict[layer]["strict_fields"])
            )

            wid.gbx_point.setVisible(dim == 0)
            wid.gbx_line.setVisible(dim == 1)
            wid.gbx_poly.setVisible(dim == 2)
            self.stack.addWidget(wid)
            self.layers_view.addItem(layer)

    def cleanupPage(self):
        """Called when user click on Back button"""
        for i in range(self.layers_view.count()):
            self.layers_view.takeItem(i)
            self.stack.removeWidget(self.stack.widget(i))
            self.conf_dict.clear()

    def exportConfig(self) -> None:
        """Save configuration file in YAML"""

        save_path = Path(self.field("save_path"))
        if save_path.suffix == "":
            save_path = save_path.with_suffix(".yaml")

        for i in range(self.stack.count()):
            wid = self.stack.widget(i)
            layer = self.layers_view.item(i).text()

            # Fields constraints
            for j in range(wid.tbl_fields.rowCount()):
                text_item = wid.tbl_fields.item(j, 0)
                chbx_item = wid.tbl_fields.item(j, 1)
                self.conf_dict[layer]["fields"][text_item.text()][
                    "mandatory"
                ] = qtCheckStateToBool(chbx_item.checkState())

            # Geometry dependent contraints
            dim = QgsWkbTypes.wkbDimensions(
                QgsWkbTypes.parseType(self.conf_dict[layer]["geometry_type"])
            )
            if dim == 0:
                pass
            elif dim == 1:
                self.conf_dict[layer]["curves"] = qtCheckStateToBool(
                    wid.chbx_curves_line.checkState()
                )
                self.conf_dict[layer]["min_length"] = wid.spb_min_length.value()
            elif dim == 2:
                self.conf_dict[layer]["holes"] = qtCheckStateToBool(
                    wid.chbx_holes.checkState()
                )
                self.conf_dict[layer]["curves"] = qtCheckStateToBool(
                    wid.chbx_curves_poly.checkState()
                )
                self.conf_dict[layer]["min_area"] = wid.spb_min_area.value()

            # Generic constraints
            self.conf_dict[layer]["strict_geometry"] = qtCheckStateToBool(
                wid.chbx_strict_geom.checkState()
            )
            self.conf_dict[layer]["strict_crs"] = qtCheckStateToBool(
                wid.chbx_strict_crs.checkState()
            )
            self.conf_dict[layer]["strict_fields"] = qtCheckStateToBool(
                wid.chbx_strict_fields.checkState()
            )

        with save_path.open("w") as of:
            yaml.dump(self.conf_dict, of)
            QMessageBox.information(
                None,
                tr("Completed"),
                tr("The configuration file has been saved to {0}").format(save_path),
            )


class ConfigPage(QWizardPage):
    """
    Page to configure the verification constraints
    while creating a new configuration
    """

    def __init__(self, parent: Optional[QWidget] = None):
        super().__init__(parent)
        self.setTitle(tr("Constraints configuration"))
        page_layout = QHBoxLayout()
        self.setLayout(page_layout)
        self.stack = QStackedWidget()
        self.layers_view = QListWidget()
        self.layers_view.setMinimumWidth(150)
        self.layers_view.setMaximumWidth(300)
        page_layout.addWidget(self.layers_view)
        page_layout.addWidget(self.stack)

        self.conf_dict: Dict[str, Any] = {}

        self.layers_view.currentRowChanged.connect(self.stack.setCurrentIndex)

    def initializePage(self):
        input_format = InputFormat(self.field("input_format"))
        filepath = self.field("filepath")

        # Shapefile
        if input_format == InputFormat.SHAPEFILES:
            info_list = list_shp_info(filepath)

        # CAD or DXF
        elif input_format == InputFormat.DXF:
            info_list = list_dxf_info(filepath)

        # GPKG
        elif input_format == InputFormat.GEOPACKAGE:
            info_list = list_gpkg_info(filepath)

        self.conf_dict["filepath"] = filepath
        self.conf_dict["input_format"] = input_format.value
        for layer, (geom_type, fields) in info_list.items():
            wid = uic.loadUi(Path(__file__).resolve(True).parent / "conf_wid.ui")
            wid.tbl_fields.setRowCount(len(fields))
            field_dict = {}
            for i, field in enumerate(fields):
                field_name = field.name()
                item_type = QTableWidgetItem(field_name)
                field_dict[field_name] = {
                    "mandatory": False,
                }
                wid.tbl_fields.setItem(i, 0, item_type)
                checkbox = QTableWidgetItem()
                checkbox.setData(Qt.CheckStateRole, Qt.Unchecked)
                checkbox.setFlags(checkbox.flags() & ~Qt.ItemIsEditable)
                wid.tbl_fields.setItem(i, 1, checkbox)
            self.conf_dict[layer] = {
                "fields": field_dict,
                "geometry_type": geom_type,
            }
            dim = QgsWkbTypes.wkbDimensions(QgsWkbTypes.parseType(geom_type))
            wid.gbx_point.setVisible(dim == 0)
            wid.gbx_line.setVisible(dim == 1)
            wid.gbx_poly.setVisible(dim == 2)
            self.stack.addWidget(wid)
            self.layers_view.addItem(layer)

    def cleanupPage(self):
        """Called when user click on Back button"""
        for i in range(self.layers_view.count()):
            self.layers_view.takeItem(i)
            self.stack.removeWidget(self.stack.widget(i))
            self.conf_dict.clear()

    def exportConfig(self) -> None:
        """Save configuration file in YAML"""

        save_path = Path(self.field("save_path"))
        if save_path.suffix == "":
            save_path = save_path.with_suffix(".yaml")

        for i in range(self.stack.count()):
            wid = self.stack.widget(i)
            layer = self.layers_view.item(i).text()

            # Fields constraints
            for j in range(wid.tbl_fields.rowCount()):
                item = wid.tbl_fields.item(j, 0)
                chbx_item = wid.tbl_fields.item(j, 1)
                self.conf_dict[layer]["fields"][item.text()][
                    "mandatory"
                ] = qtCheckStateToBool(chbx_item.checkState())

            # Geometry dependent contraints
            dim = QgsWkbTypes.wkbDimensions(
                QgsWkbTypes.parseType(self.conf_dict[layer]["geometry_type"])
            )
            if dim == 0:
                pass
            elif dim == 1:
                self.conf_dict[layer]["curves"] = qtCheckStateToBool(
                    wid.chbx_curves_line.checkState()
                )
                self.conf_dict[layer]["min_length"] = wid.spb_min_length.value()
            elif dim == 2:
                self.conf_dict[layer]["holes"] = qtCheckStateToBool(
                    wid.chbx_holes.checkState()
                )
                self.conf_dict[layer]["curves"] = qtCheckStateToBool(
                    wid.chbx_curves_poly.checkState()
                )
                self.conf_dict[layer]["min_area"] = wid.spb_min_area.value()

            # Generic constraints
            self.conf_dict[layer]["strict_geometry"] = qtCheckStateToBool(
                wid.chbx_strict_geom.checkState()
            )
            self.conf_dict[layer]["strict_crs"] = qtCheckStateToBool(
                wid.chbx_strict_crs.checkState()
            )
            self.conf_dict[layer]["strict_fields"] = qtCheckStateToBool(
                wid.chbx_strict_fields.checkState()
            )

        with save_path.open("w") as of:
            yaml.dump(self.conf_dict, of)
            QMessageBox.information(
                None,
                tr("Completed"),
                tr("The configuration file has been saved to {0}").format(save_path),
            )


class SaveConfigPage(QWizardPage):
    """
    Page to save a configuration (while editing an existing configuration
    or creating a new one)
    """

    def __init__(self, parent: Optional[QWidget] = None):
        super().__init__(parent)
        self.setTitle(tr("Save configuration file"))
        self.setSubTitle(tr("Choose the configuration file location and name"))
        page_layout = QVBoxLayout()
        self.setLayout(page_layout)
        self.file_widget = QgsFileWidget()
        self.file_widget.setFilter("YAML (*.yaml)")
        self.file_widget.setStorageMode(QgsFileWidget.SaveFile)
        page_layout.addWidget(self.file_widget)
        self.registerField("save_path*", self.file_widget.lineEdit())

    def initializePage(self):
        self.file_widget.lineEdit().setText(self.field("config_file"))

    def validatePage(self):
        """We want the extension to be empty or .yaml"""

        filepath = Path(self.field("save_path"))
        if filepath.suffix not in [".yaml", ".YAML", ""]:
            QMessageBox.warning(
                None, tr("Warning"), tr("The file extension must be .yaml")
            )
            return False
        return True


class ConfigInputPage(QWizardPage):
    """
    Page to load a configuration file to edit the configuration
    or to launch a verification
    """

    def __init__(self, parent=None):
        super().__init__(parent)
        file_widget = QgsFileWidget()
        file_widget.setFilter("YAML (*.yaml)")
        self.setLayout(QVBoxLayout())
        self.layout().addWidget(file_widget)
        self.setTitle(tr("Load the configuration file"))
        self.registerField("config_file*", file_widget.lineEdit())

    def validatePage(self):
        config_file = Path(self.field("config_file"))
        if not config_file.exists():
            QMessageBox.warning(
                None,
                tr("Warning"),
                tr("The configuration file {0} does not exist!").format(config_file),
            )
            return False

        with Path(self.field("config_file")).open() as filepath:
            conf_dict = yaml.full_load(filepath)

        check = check_conf_dict_format(conf_dict)
        if check != "":
            QMessageBox.warning(None, tr("Bad configuration file!"), check)
            return False

        ref_filepath = conf_dict["filepath"]
        if not Path(ref_filepath).exists():
            QMessageBox.critical(
                None,
                tr("Error"),
                tr(
                    f"The path {ref_filepath} referenced in the config file does not exist!"
                ),
            )
            return False

        return True


class VerifPage(QWizardPage):
    """
    Page to perform the verification
    """

    def __init__(self, parent=None):
        super().__init__(parent)
        self.setLayout(QVBoxLayout())
        self.text_report = QTextEdit()
        self.text_report.setReadOnly(True)
        self.layout().addWidget(self.text_report)
        self.setTitle(tr("Report"))

    def initializePage(self):
        self.text_report.clear()
        with Path(self.field("config_file")).open() as filepath:
            conf_dict = yaml.full_load(filepath)
        self.worker = DoVerif(conf_dict, self.field("filepath"))
        self.worker.log_msg.connect(self.text_report.append)
        QgsApplication.taskManager().addTask(self.worker)


class SaveReportPage(QWizardPage):
    """
    Page to save the verification report
    """

    def __init__(self, parent: Optional[QWidget] = None):
        super().__init__(parent)
        self.setTitle(tr("Save report"))
        self.setSubTitle(tr("Choose the path and format of the report"))
        page_layout = QVBoxLayout()
        self.setLayout(page_layout)
        report_path = QgsFileWidget()
        report_path.setStorageMode(QgsFileWidget.SaveFile)
        cbx_report_format = QComboBox()
        cbx_report_format.addItems(["", "Markdown", "HTML"])
        page_layout.addWidget(cbx_report_format)
        page_layout.addWidget(report_path)
        self.registerField("report_format*", cbx_report_format)
        self.registerField("report_path*", report_path.lineEdit())

        def textChanged(text: str) -> None:
            """
            Change the behavior or the file selector according to
            the file type
            """
            if text == "Markdown":
                report_path.setFilter("Markdown (*.md)")
            elif text == "HTML":
                report_path.setFilter("HTML (*.html)")
            elif text == "":
                report_path.setFilter("")
            else:
                raise ValueError(text + "is not a valid input")

        textChanged(cbx_report_format.currentText())
        cbx_report_format.currentTextChanged.connect(textChanged)

    def save(self, report):
        with Path(self.field("report_path")).open("w") as fileout:
            if self.field("report_format") == 1:
                fileout.write(report.report(ReportFormat.MARKDOWN))
            elif self.field("report_format") == 2:
                fileout.write(report.report(ReportFormat.HTML))
            else:
                raise ValueError(self.field("report_format") + "is not a valid input")
        QMessageBox.information(
            None,
            tr("Finished"),
            tr(
                f"<html>The report has correctly been saved to <a href=\"{self.field('report_path')}\">{self.field('report_path')}</a></html>"
            ),
        )


class ConfigurationCreationWizard(QWizard):
    """
    Wizard to create a configuration file
    """

    def __init__(self, parent=Optional[QWidget]):
        super().__init__()
        self.setWindowTitle(tr("Configuration creation"))
        data_input_page = DataInputPage()
        config_page = ConfigPage()
        save_page = SaveConfigPage()
        self.addPage(data_input_page)
        self.addPage(config_page)
        self.addPage(save_page)
        self.accepted.connect(config_page.exportConfig)


class ConfigurationEditWizard(QWizard):
    """
    Wizard to edit an existing configuration file
    """

    def __init__(self, parent=Optional[QWidget]):
        super().__init__()
        self.setWindowTitle(tr("Configuration edition"))
        input_page = ConfigInputPage()
        input_page.setButtonText(QWizard.NextButton, tr("Edit configuration"))
        config_page = ConfigEditPage()
        save_page = SaveConfigPage()
        self.addPage(input_page)
        self.addPage(config_page)
        self.addPage(save_page)
        self.accepted.connect(config_page.exportConfig)


class CheckComplianceWizard(QWizard):
    """
    Wizard to perform a verification
    """

    def __init__(self, parent=Optional[QWidget]):
        super().__init__()
        self.setWindowTitle(tr("Check compliance"))
        config_page = ConfigInputPage()
        config_page.setButtonText(QWizard.NextButton, tr("Check compliance"))
        data_input_page = VerifDataInputPage()
        reporting_page = VerifPage()
        save_report_page = SaveReportPage()
        self.addPage(config_page)
        self.addPage(data_input_page)
        self.addPage(reporting_page)
        self.addPage(save_report_page)
        self.accepted.connect(
            lambda: save_report_page.save(reporting_page.worker.run_report)
        )
