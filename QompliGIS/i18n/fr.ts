<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR" sourcelanguage="en_US">
<context>
    <name>@default</name>
    <message>
        <location filename="../qompligis.py" line="47"/>
        <source>Create a template</source>
        <translation>Créer une configuration</translation>
    </message>
    <message>
        <location filename="../qompligis.py" line="53"/>
        <source>Edit a template</source>
        <translation>Éditer une configuration</translation>
    </message>
    <message>
        <location filename="../qompligis.py" line="59"/>
        <source>Compliance check</source>
        <translation>Vérification de conformité</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="72"/>
        <source>Choose a file</source>
        <translation>Choisir un fichier</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="69"/>
        <source>Choose a folder</source>
        <translation>Choisir un dossier</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="78"/>
        <source>Format</source>
        <translation>Format</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="475"/>
        <source>Warning</source>
        <translation>Attention</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="300"/>
        <source>Constraints configuration</source>
        <translation>Configuration des contraintes</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="417"/>
        <source>Completed</source>
        <translation>Terminé</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="432"/>
        <source>Save configuration file</source>
        <translation>Enregistrer le fichier de configuration</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="433"/>
        <source>Choose the configuration file location and name</source>
        <translation>Choisir l&apos;emplacement et le nom du fichier de configuration</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="450"/>
        <source>The file extension must be .yaml</source>
        <translation>L&apos;extension doit être .yaml</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="469"/>
        <source>Load the configuration file</source>
        <translation>Charger le fichier de configuration</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="487"/>
        <source>Bad configuration file!</source>
        <translation>Fichier de configuration invalide !</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="492"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="492"/>
        <source>The path {ref_filepath} referenced in the config file does not exist!</source>
        <translation>Le chemin {ref_filepath} référencé dans le fichier de configuration n&apos;existe pas !</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="515"/>
        <source>Report</source>
        <translation>Rapport</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="533"/>
        <source>Save report</source>
        <translation>Enregistrer le rapport</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="534"/>
        <source>Choose the path and format of the report</source>
        <translation>Choisir l&apos;emplacement et le format du rapport</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="571"/>
        <source>Finished</source>
        <translation>Terminé</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="571"/>
        <source>&lt;html&gt;The report has correctly been saved to &lt;a href=&quot;{self.field(&apos;report_path&apos;)}&quot;&gt;{self.field(&apos;report_path&apos;)}&lt;/a&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;Le rapport a été correcement sauvegardé ici : &lt;a href=&quot;{self.field(&apos;report_path&apos;)}&quot;&gt;{self.field(&apos;report_path&apos;)}&lt;/a&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="587"/>
        <source>Configuration creation</source>
        <translation>Création de la configuration</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="52"/>
        <source>Choose the dataset to use as a reference</source>
        <translation>Choisir les données à utiliser comme référence</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="604"/>
        <source>Configuration edition</source>
        <translation>Modification de la configuration</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="606"/>
        <source>Edit configuration</source>
        <translation>Modifier la configuration</translation>
    </message>
    <message>
        <location filename="../processing.py" line="103"/>
        <source>Check compliance</source>
        <translation>Vérifier la conformité</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="108"/>
        <source>Choose the dataset to check</source>
        <translation>Choisir les données à vérifier</translation>
    </message>
    <message>
        <location filename="../utils.py" line="645"/>
        <source>The path {0} does not exist!</source>
        <translation>Le chemin {0} n&apos;existe pas !</translation>
    </message>
    <message>
        <location filename="../utils.py" line="684"/>
        <source>No layer found in reference file!</source>
        <translation>Pas de couche trouvée dans le fichier de référence !</translation>
    </message>
    <message>
        <location filename="../utils.py" line="688"/>
        <source>Checking layer list...</source>
        <translation>Vérification de la liste de couches...</translation>
    </message>
    <message>
        <location filename="../utils.py" line="691"/>
        <source>OK: Layers comply with the reference</source>
        <translation>OK : les couches sont conformes à la référence</translation>
    </message>
    <message>
        <location filename="../utils.py" line="697"/>
        <source>Layer &apos;{0}&apos; is missing in dataset</source>
        <translation>La couche &apos;{0}&apos; est absente des données d&apos;entrée</translation>
    </message>
    <message>
        <location filename="../utils.py" line="701"/>
        <source>Layer &apos;{0}&apos; found but not present in reference dataset</source>
        <translation>La couche &apos;{0}&apos; a été trouvée, mais est absente des données de référence</translation>
    </message>
    <message>
        <location filename="../utils.py" line="717"/>
        <source>Checking layer {0}</source>
        <translation>Vérification de la couche {0}</translation>
    </message>
    <message>
        <location filename="../utils.py" line="721"/>
        <source>Checking CRS...</source>
        <translation>Vérification du SCR...</translation>
    </message>
    <message>
        <location filename="../utils.py" line="745"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../utils.py" line="728"/>
        <source>CRS is {new_crs} but must be {ref_crs}</source>
        <translation>Le SRC est {new_crs} mais doit être {ref_crs}</translation>
    </message>
    <message>
        <location filename="../utils.py" line="734"/>
        <source>No CRS check asked</source>
        <translation>Pas de vérification du SCR demandée</translation>
    </message>
    <message>
        <location filename="../utils.py" line="740"/>
        <source>Checking fields...</source>
        <translation>Vérification des champs...</translation>
    </message>
    <message>
        <location filename="../utils.py" line="751"/>
        <source>Differences found on fields: see each field information.</source>
        <translation>Différences trouvées sur les champs : voir le détail de chaque champ</translation>
    </message>
    <message>
        <location filename="../utils.py" line="756"/>
        <source>Missing</source>
        <translation>Manquant</translation>
    </message>
    <message>
        <location filename="../utils.py" line="758"/>
        <source> - AND MANDATORY!</source>
        <translation> - ET OBLIGATOIRE !</translation>
    </message>
    <message>
        <location filename="../utils.py" line="760"/>
        <source>Not in reference layer.</source>
        <translation>Absent de la couche de référence</translation>
    </message>
    <message>
        <location filename="../utils.py" line="764"/>
        <source>Fields are in a different order: found {0} but reference layer order is {1}.</source>
        <translation>Les champs sont dans un ordre différent : {0} trouvé mais l&apos;ordre de référence est {1}.</translation>
    </message>
    <message>
        <location filename="../utils.py" line="768"/>
        <source>Fields are all present and in the same order: see each field information.</source>
        <translation>Les champs sont tous présents et dans le même ordre : voir le détail de chaque champ.</translation>
    </message>
    <message>
        <location filename="../utils.py" line="777"/>
        <source>Length differ: found {0} but reference is {1}.</source>
        <translation>La longueur diffère : trouvé {0} mais la référence est {1}.</translation>
    </message>
    <message>
        <location filename="../utils.py" line="782"/>
        <source>Precision differ: found {0} but reference is {1}.</source>
        <translation>La précision diffère : trouvée {0} mais la référence est {1}.</translation>
    </message>
    <message>
        <location filename="../utils.py" line="787"/>
        <source>Type differ: found (type: {0}, length: {1}, precision: {2}) but reference is (type: {3}, length: {4}, precision: {5}).</source>
        <translation>Le type diffère : trouvé (type: {0}, longueur: {1}, précision: {2}) mais la référence est (type: {3}, longueur: {4}, précision: {5}).</translation>
    </message>
    <message>
        <location filename="../utils.py" line="800"/>
        <source>Other difference... found informations are type: {0}, length: {1}, precision: {2} and reference informations are type: {3}, length: {4}, precision: {5}.</source>
        <translation>Autre différence... informations trouvées : type: {0}, longueur: {1}, précision: {2} et les informations de référence sont : type: {3}, longueur: {4}, précision: {5}.</translation>
    </message>
    <message>
        <location filename="../utils.py" line="812"/>
        <source>Fields are the same.</source>
        <translation>Les champs sont identiques.</translation>
    </message>
    <message>
        <location filename="../utils.py" line="817"/>
        <source>No strict check on fields</source>
        <translation>Pas de vérification stricte sur les champs.</translation>
    </message>
    <message>
        <location filename="../utils.py" line="840"/>
        <source>Checking minimum length...</source>
        <translation>Vérification de la longueur minimale...</translation>
    </message>
    <message>
        <location filename="../utils.py" line="843"/>
        <source>No minimum length check asked</source>
        <translation>Pas de vérification de longueur minimale demandée.</translation>
    </message>
    <message>
        <location filename="../utils.py" line="849"/>
        <source>Checking minimum area...</source>
        <translation>Vérification de l&apos;aire minimale...</translation>
    </message>
    <message>
        <location filename="../utils.py" line="852"/>
        <source>No minimum area check asked</source>
        <translation>Pas de vérification d&apos;aire minimale demandée.</translation>
    </message>
    <message>
        <location filename="../utils.py" line="858"/>
        <source>Checking curves...</source>
        <translation>Vérification des courbes...</translation>
    </message>
    <message>
        <location filename="../utils.py" line="860"/>
        <source>No curves check asked</source>
        <translation>Pas de vérification de courbes demandée</translation>
    </message>
    <message>
        <location filename="../utils.py" line="866"/>
        <source>Checking holes...</source>
        <translation>Vérification des trous...</translation>
    </message>
    <message>
        <location filename="../utils.py" line="868"/>
        <source>No holes check asked</source>
        <translation>Pas de vérification de trous demandée</translation>
    </message>
    <message>
        <location filename="../utils.py" line="880"/>
        <source>Feature {0} has a length {1} &lt; minimum length {2}</source>
        <translation>L&apos;entité {0} a une longueur de {1} &lt; longueur minimum {2}</translation>
    </message>
    <message>
        <location filename="../utils.py" line="890"/>
        <source>Feature {0} has an area {1} &lt; minimum area {2}</source>
        <translation>L&apos;entité {0} a une aire de {1} &lt; aire minimum {2}</translation>
    </message>
    <message>
        <location filename="../utils.py" line="900"/>
        <source>Feature {0} contains curve(s)</source>
        <translation>L&apos;entité {0} contient une (des) courbe(s)</translation>
    </message>
    <message>
        <location filename="../utils.py" line="908"/>
        <source>Feature {0} contains hole(s)</source>
        <translation>L&apos;entité {0} contient un (des) trou(s)</translation>
    </message>
    <message>
        <location filename="../utils.py" line="934"/>
        <source>OK: layer complies with minimum length</source>
        <translation>OK : la couche est conforme à la longueur minimum</translation>
    </message>
    <message>
        <location filename="../utils.py" line="939"/>
        <source>OK: layer complies with minimum area</source>
        <translation>OK : la couche est conforme à l&apos;aire minimum</translation>
    </message>
    <message>
        <location filename="../utils.py" line="944"/>
        <source>OK: layer does not have curve</source>
        <translation>OK : la couche n&apos;a pas de courbe</translation>
    </message>
    <message>
        <location filename="../utils.py" line="949"/>
        <source>OK: layer does not have hole</source>
        <translation>OK : la couche n&apos;a pas de trou</translation>
    </message>
    <message>
        <location filename="../report.py" line="34"/>
        <source>Layer list</source>
        <translation>Liste de couches</translation>
    </message>
    <message>
        <location filename="../report.py" line="36"/>
        <source>CRS</source>
        <translation>SCR</translation>
    </message>
    <message>
        <location filename="../report.py" line="37"/>
        <source>Fields</source>
        <translation>Champs</translation>
    </message>
    <message>
        <location filename="../report.py" line="38"/>
        <source>Curves</source>
        <translation>Courbes</translation>
    </message>
    <message>
        <location filename="../report.py" line="39"/>
        <source>Holes</source>
        <translation>Trous</translation>
    </message>
    <message>
        <location filename="../report.py" line="40"/>
        <source>Minimum length</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../report.py" line="41"/>
        <source>Minimum area</source>
        <translation>Aire minimum</translation>
    </message>
    <message>
        <location filename="../report.py" line="60"/>
        <source>For FIELDS section, use addFieldInfo() method</source>
        <translation>Pour la section FIELDS, utiliser la méthode addFieldInfo()</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="120"/>
        <source>Choose a folder with your shapefiles</source>
        <translation>Choisir un dossier avec vos shpesfiles</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="123"/>
        <source>Choose a file - {0}</source>
        <translation>Choisir un fichier - {0}</translation>
    </message>
    <message>
        <location filename="../utils.py" line="991"/>
        <source>Bad formed configuration file: missing section(s) {0}</source>
        <translation>Fichier de configuration malformé : section(s) manquante(s) {0}</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="417"/>
        <source>The configuration file has been saved to {0}</source>
        <translation>Le fichier de configuration a été enregistré vers {0}</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="475"/>
        <source>The configuration file {0} does not exist!</source>
        <translation>Le fichier de configuration {0} n&apos;existe pas !</translation>
    </message>
    <message>
        <location filename="../utils.py" line="662"/>
        <source>The data format to check ({0}) does not correspond with the reference data ({1})</source>
        <translation>Le format de donnée à vérifier ({0}) ne correspond pas au format des données de référence ({1})</translation>
    </message>
    <message>
        <location filename="../report.py" line="66"/>
        <source>The layer {0} is not present in the report</source>
        <translation>La couche {0} est absente dans le rapport</translation>
    </message>
    <message>
        <location filename="../processing.py" line="25"/>
        <source>Configuration file</source>
        <translation>Fichier de configuration</translation>
    </message>
    <message>
        <location filename="../processing.py" line="34"/>
        <source>Data folder to check (for Shapefiles)</source>
        <translation>Dossier de données à varifier (pour Shapefiles)</translation>
    </message>
    <message>
        <location filename="../processing.py" line="42"/>
        <source>Data file to check</source>
        <translation>Fichier de données à vérifier</translation>
    </message>
    <message>
        <location filename="../processing.py" line="51"/>
        <source>Report format</source>
        <translation>Format du rapport</translation>
    </message>
    <message>
        <location filename="../processing.py" line="60"/>
        <source>Report path</source>
        <translation>Chemin du rapport</translation>
    </message>
    <message>
        <location filename="../processing.py" line="78"/>
        <source>A data file or a data folder to check (for Shapefiles) must be supplied</source>
        <translation>Un fichier de données ou un dossier de données à vérifier (pour Shapefiles) doit être renseigné</translation>
    </message>
    <message>
        <location filename="../processing.py" line="95"/>
        <source>Saved to </source>
        <translation>Sauvegardé vers</translation>
    </message>
    <message>
        <location filename="../utils.py" line="762"/>
        <source>Field {0}: {1}</source>
        <translation>Champ {0} : {1}</translation>
    </message>
    <message>
        <location filename="../utils.py" line="874"/>
        <source>Checking NULL fields...</source>
        <translation>Vérification des champs NULL</translation>
    </message>
    <message>
        <location filename="../utils.py" line="923"/>
        <source>Mandatory field is NULL in feature {0}</source>
        <translation>Champ obligatoire à NULL dans l&apos;entité {0}</translation>
    </message>
</context>
<context>
    <name>Form</name>
    <message>
        <location filename="../conf_wid.ui" line="20"/>
        <source>Which types of compliances do you want to configure ?</source>
        <translation>Quelles types de conformités voulez vous configurer ?</translation>
    </message>
    <message>
        <location filename="../conf_wid.ui" line="27"/>
        <source>Strict geometry type (Multi vs single, Z, M, etc)</source>
        <translation>Type de géométrie strict (Multi vs single, Z, M, etc)</translation>
    </message>
    <message>
        <location filename="../conf_wid.ui" line="37"/>
        <source>Don&apos;t allow a different coordinate reference system</source>
        <translation>Ne pas autoriser un système de coordonnées différent</translation>
    </message>
    <message>
        <location filename="../conf_wid.ui" line="44"/>
        <source>Don&apos;t allow new attributes</source>
        <translation>Ne pas autoriser de nouveaux attributs</translation>
    </message>
    <message>
        <location filename="../conf_wid.ui" line="52"/>
        <source>Attributes</source>
        <translation>Attributs</translation>
    </message>
    <message>
        <location filename="../conf_wid.ui" line="57"/>
        <source>Mandatory</source>
        <translation>Obligatoire</translation>
    </message>
    <message>
        <location filename="../conf_wid.ui" line="65"/>
        <source>Point layer options</source>
        <translation>Options de couche type Point</translation>
    </message>
    <message>
        <location filename="../conf_wid.ui" line="71"/>
        <source>No option yet</source>
        <translation>Pas d&apos;option pour le moment</translation>
    </message>
    <message>
        <location filename="../conf_wid.ui" line="81"/>
        <source>Line layer options</source>
        <translation>Options de couche type Ligne</translation>
    </message>
    <message>
        <location filename="../conf_wid.ui" line="131"/>
        <source>Can contain curves</source>
        <translation>Peut contenir des courbes</translation>
    </message>
    <message>
        <location filename="../conf_wid.ui" line="94"/>
        <source>Minimum length</source>
        <translation>Longueur minimale</translation>
    </message>
    <message>
        <location filename="../conf_wid.ui" line="111"/>
        <source>Polygon layer options</source>
        <translation>Option de couche type Polygone</translation>
    </message>
    <message>
        <location filename="../conf_wid.ui" line="124"/>
        <source>Minimum area</source>
        <translation>Aire minimum</translation>
    </message>
    <message>
        <location filename="../conf_wid.ui" line="138"/>
        <source>Can contain holes</source>
        <translation>Peut contenir des trous</translation>
    </message>
    <message>
        <location filename="../conf_wid.ui" line="14"/>
        <source>Configuration</source>
        <translation>Configuration</translation>
    </message>
</context>
</TS>
