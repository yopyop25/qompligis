"""
QompliGIS providers
"""

from qgis.core import QgsProcessingProvider  # pylint: disable=import-error

from QompliGIS.processing import QompliGISProcessing


class QompliGISProvider(QgsProcessingProvider):
    """
    QGIS Provider adding all QompliGIS Providers
    """

    def __init__(self):
        QgsProcessingProvider.__init__(self)

    def unload(self):
        """
        Unloads the provider. Any tear-down steps required by the provider
        should be implemented here.
        """
        pass  # pylint: disable=unnecessary-pass

    def loadAlgorithms(self):
        """
        Loads all algorithms belonging to this provider.
        """
        self.addAlgorithm(QompliGISProcessing())

    # pylint: disable=no-self-use
    def id(self):
        """
        Returns the unique provider id, used for identifying the provider. This
        string should be a unique, short, character only string, eg "qgis" or
        "gdal". This string should not be localised.
        """
        return "qompligis"

    def name(self):
        """
        Returns the provider name, which is used to describe the provider
        within the GUI.

        This string should be short (e.g. "Lastools") and localised.
        """
        return "QompliGIS"

    def longName(self):
        """
        Returns the a longer version of the provider name, which can include
        extra details such as version numbers. E.g. "Lastools LIDAR tools
        (version 2.2.1)". This string should be localised. The default
        implementation returns the same string as name().
        """
        return self.name()
