from pathlib import Path

from qgis.core import Qgis, QgsApplication
from qgis.PyQt.QtCore import QCoreApplication, QSettings, QTranslator
from qgis.PyQt.QtGui import QIcon
from qgis.PyQt.QtWidgets import QAction, QToolBar

from QompliGIS import resources_rc
from QompliGIS.gui_utils import (
    CheckComplianceWizard,
    ConfigurationCreationWizard,
    ConfigurationEditWizard,
)
from QompliGIS.provider import QompliGISProvider
from QompliGIS.qt_utils import tr


class QompliGIS:
    def __init__(self, iface):

        # initialize locale
        locale = QSettings().value("locale/userLocale")
        if locale is not None:
            locale = locale[0:2]
            locale_path = Path(__file__).parent / "i18n" / f"{locale}.qm"
            if locale_path.exists():
                translator = QTranslator(QCoreApplication.instance())
                if translator.load(str(locale_path)):
                    QCoreApplication.installTranslator(translator)
                else:
                    iface.messageBar().pushMessage(
                        f"{locale} translation failed to load", Qgis.Warning
                    )
            else:
                iface.messageBar().pushMessage(
                    f"No translation file for {locale}", Qgis.Info
                )

        self.iface = iface
        self.wizard = None

        self.provider = QompliGISProvider()

    def initGui(self):
        self.toolbar = QToolBar("QompliGIS")
        self.toolbar.setObjectName("QompliGIS-toolbar")
        self.create_configuration_action = QAction(
            QIcon(":/new_config"), tr("Create a template")
        )
        self.create_configuration_action.triggered.connect(self.createConfiguration)
        self.toolbar.addAction(self.create_configuration_action)

        self.edit_configuration_action = QAction(
            QIcon(":/edit_config"), tr("Edit a template")
        )
        self.edit_configuration_action.triggered.connect(self.editConfiguration)
        self.toolbar.addAction(self.edit_configuration_action)

        self.compliance_check_action = QAction(
            QIcon(":/check_compliance"), tr("Compliance check")
        )
        self.compliance_check_action.triggered.connect(self.checkCompliance)
        self.toolbar.addAction(self.compliance_check_action)
        self.iface.addToolBar(self.toolbar)

        QgsApplication.processingRegistry().addProvider(self.provider)

    def unload(self):
        self.iface.helpToolBar().parentWidget().removeToolBar(self.toolbar)
        del self.toolbar
        del self.compliance_check_action
        del self.create_configuration_action
        del self.edit_configuration_action

        QgsApplication.processingRegistry().removeProvider(self.provider)

    def checkCompliance(self):
        self.wizard = CheckComplianceWizard()
        self.wizard.show()

    def createConfiguration(self):
        self.wizard = ConfigurationCreationWizard()
        self.wizard.show()

    def editConfiguration(self):
        self.wizard = ConfigurationEditWizard()
        self.wizard.show()
