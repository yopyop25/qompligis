from typing import cast

from qgis.PyQt.QtCore import QCoreApplication, Qt


def boolToQtCheckState(constraint: bool) -> Qt.CheckState:
    return Qt.Checked if constraint else Qt.Unchecked


def qtCheckStateToBool(check_state: Qt.CheckState) -> bool:
    return cast(bool, check_state == Qt.Checked)


def tr(text: str) -> str:
    return cast(str, QCoreApplication.translate("@default", text))
