"""
This file contains the report class
"""

from enum import Enum, unique
from typing import Any, Dict, List

from QompliGIS.qt_utils import tr


@unique
class ReportFormat(Enum):
    HTML = 0
    MARKDOWN = 1


@unique
class LayerSection(Enum):
    CRS = 0
    FIELDS = 1
    CURVES = 2
    HOLES = 3
    MIN_LENGTH = 4
    MIN_AREA = 5


@unique
class MainSection(Enum):
    LAYER_LIST = 0


class QompliGISReport:
    def __init__(self):
        self.main_sections = [tr("Layer list")]
        self.layer_sections = [
            tr("CRS"),
            tr("Fields"),
            tr("Curves"),
            tr("Holes"),
            tr("Minimum length"),
            tr("Minimum area"),
        ]
        self.layers = []
        self.report_dict: Dict[str, Any] = {
            main_section: list() for main_section in self.main_sections
        }

    def addLayer(self, layer_name: str) -> None:
        self.report_dict[layer_name] = {
            section: list() for section in self.layer_sections
        }
        self.report_dict[layer_name][
            self.layer_sections[LayerSection.FIELDS.value]
        ] = dict()

    def addLayerSection(
        self, layer_name: str, layer_section: LayerSection, info: str
    ) -> None:
        if layer_section == LayerSection.FIELDS:
            raise ValueError(tr("For FIELDS section, use addFieldInfo() method"))
        try:
            self.report_dict[layer_name][
                self.layer_sections[layer_section.value]
            ].append(info)
        except KeyError as err:
            raise KeyError(
                tr("The layer {0} is not present in the report").format(layer_name)
            ) from err

    def addMainSection(self, section: MainSection, info: str) -> None:
        self.report_dict[self.main_sections[section.value]].append(info)

    def addFieldInfo(self, layer_name: str, field_name: str, info: str) -> None:
        fields_dict = self.report_dict[layer_name][
            self.layer_sections[LayerSection.FIELDS.value]
        ]
        try:
            fields_dict[field_name].append(info)
        except KeyError:
            fields_dict[field_name] = [info]

    def report(self, report_format: ReportFormat) -> str:
        report = ""

        for key, value in self.report_dict.items():
            if key in self.main_sections:
                report += formatTitle(report_format, key)
                report += formatContent(report_format, value)
            else:
                report += formatTitle(report_format, f'Layer "{key}"')
                for layer_section in self.layer_sections:
                    if len(value[layer_section]) == 0:
                        continue
                    report += formatSection(report_format, layer_section)
                    if layer_section == self.layer_sections[LayerSection.FIELDS.value]:
                        if "" in value[layer_section]:
                            info = value[layer_section].pop("")
                            report += formatContent(report_format, info)
                        for field, info in value[layer_section].items():
                            report += formatField(report_format, field)
                            report += formatContent(report_format, info)
                    else:
                        report += formatContent(report_format, value[layer_section])

        return report


def formatTitle(report_format: ReportFormat, text: str) -> str:
    if report_format == ReportFormat.HTML:
        return "<h1>" + text + "</h1>"
    if report_format == ReportFormat.MARKDOWN:
        return "# " + text + formatNewLine(report_format)
    raise NotImplementedError()


def formatSection(report_format: ReportFormat, text: str) -> str:
    if report_format == ReportFormat.HTML:
        return "<h2>" + text + "</h2>"
    if report_format == ReportFormat.MARKDOWN:
        return "## " + text + formatNewLine(report_format)
    raise NotImplementedError()


def formatField(report_format: ReportFormat, text: str) -> str:
    if report_format == ReportFormat.HTML:
        return "<h3>" + text + "</h3>"
    if report_format == ReportFormat.MARKDOWN:
        return "### " + text + formatNewLine(report_format)
    raise NotImplementedError()


def formatContent(report_format: ReportFormat, text: List[str]) -> str:
    if report_format == ReportFormat.HTML:
        return formatNewLine(report_format).join(text)
    if report_format == ReportFormat.MARKDOWN:
        return formatNewLine(report_format).join(text) + formatNewLine(report_format)
    raise NotImplementedError()


def formatBold(report_format: ReportFormat, text: str) -> str:
    if report_format == ReportFormat.HTML:
        return "<bold>" + text + "</bold>"
    if report_format == ReportFormat.MARKDOWN:
        return "**" + text + "**"
    raise NotImplementedError()


def formatNewLine(report_format: ReportFormat) -> str:
    """formatNewLine.

    Returns the new line character for the desired format

    Parameters
    ----------
    report_format : ReportFormat

    Returns
    -------
    str

    """
    if report_format == ReportFormat.HTML:
        return "<br>"
    if report_format == ReportFormat.MARKDOWN:
        return "\n\n"
    raise NotImplementedError()
